﻿using Microsoft.Extensions.Caching.Memory;
using TemperatureCaptor.Application.Dtos;

namespace TemperatureCaptor.Application.Services;

public class CacheService : ICacheService
{
    private readonly IMemoryCache _memoryCache;

    public CacheService(IMemoryCache memoryCache) => _memoryCache = memoryCache;


    public void AddTemperature(TemperatureDto temperature)
    {
        var listCache = _memoryCache.Get<List<TemperatureDto>>("temperatures") ?? new List<TemperatureDto>(15);
        if (listCache.Count == 15)
        {
            var firstTemp = listCache.First();
            listCache.Remove(firstTemp);
        }

        listCache.Add(temperature);
        _memoryCache.Set("temperatures", listCache);
    }

    public List<TemperatureDto> GetTemperatureCache()
    {
        return _memoryCache.Get<List<TemperatureDto>>("temperatures") ?? new List<TemperatureDto>();
    }
}
