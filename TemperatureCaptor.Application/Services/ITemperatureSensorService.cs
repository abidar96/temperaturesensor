﻿using TemperatureCaptor.Application.Dtos;

namespace TemperatureCaptor.Application.Services;

public interface ITemperatureSensorService
{
    TemperatureDto GetTemperature();

    void ChangeLimits(int min, int max);
}
