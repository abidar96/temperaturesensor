﻿using TemperatureCaptor.Application.Dtos;

namespace TemperatureCaptor.Application.Services;

public interface ICacheService
{
    List<TemperatureDto> GetTemperatureCache();

    void AddTemperature(TemperatureDto temperature);
}
