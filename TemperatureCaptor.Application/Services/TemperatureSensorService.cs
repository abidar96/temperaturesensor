﻿using TemperatureCaptor.Application.Dtos;
using TemperatureCaptor.Domain.Entities;
using TemperatureCaptor.Domain.Services;
using TemperatureCaptor.Infrastructure.TemperatureCaptorGenerator;

namespace TemperatureCaptor.Application.Services;

public class TemperatureSensorService : ITemperatureSensorService
{
    private readonly ITemperatureCaptorGenerator _temperatureCaptor;
    private readonly ITemperatureService _temperatureService;

    public TemperatureSensorService(ITemperatureCaptorGenerator temperatureCaptor, ITemperatureService itemperatureService)
    {
        _temperatureCaptor = temperatureCaptor;
        _temperatureService = itemperatureService;
    }

    public TemperatureDto GetTemperature()
    {
        var value = _temperatureCaptor.GetTemperature();
        var temperature = new Temperature()
        {
            Value = value,
            Status = _temperatureService.GetTemperatureStatus(value),
        };

        return TemperatureDto.FromTemperature(temperature);
    }

    public void ChangeLimits(int min,int max)
    {
        _temperatureService.Limits = (min,max);
        return;
    }
}
