using TemperatureCaptor.Application.Services;
using TemperatureCaptor.Domain.Services;
using TemperatureCaptor.Infrastructure.Services;
using TemperatureCaptor.Infrastructure.TemperatureCaptorGenerator;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddSwaggerGen();

builder.Services.AddMemoryCache();

builder.Services.AddSingleton<ITemperatureCaptorGenerator, TemperatureCaptorGenerator>()
    .AddSingleton<ITemperatureService, TemperatureService>()
    .AddScoped<ITemperatureSensorService, TemperatureSensorService>()
    .AddScoped<ICacheService,CacheService>();



var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
namespace TemperatureCaptor.Application
{
    public partial class Program { }

}
