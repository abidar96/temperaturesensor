﻿using TemperatureCaptor.Domain.Entities;

namespace TemperatureCaptor.Application.Dtos;

public class TemperatureDto
{
    public int Value { get; set; }

    public string Status { get; set; }

    public static TemperatureDto FromTemperature(Temperature temperature)
    {
        return new TemperatureDto()
        {
            Value = temperature.Value,
            Status = temperature.Status.ToString(),
        };
    }
}
