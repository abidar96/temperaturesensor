﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TemperatureCaptor.Application.Dtos;
using TemperatureCaptor.Application.Services;

namespace TemperatureCaptor.Application.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TemperatureController : ControllerBase
{
    private readonly ITemperatureSensorService _temperatureSensorService;
    private readonly ICacheService _cacheService;

    public TemperatureController(ITemperatureSensorService temperatureSensorService, ICacheService cacheService)
    { 
        _temperatureSensorService = temperatureSensorService;
        _cacheService = cacheService;

    }

    [SwaggerOperation(Summary ="Get Current Temperature")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(TemperatureDto),200)]
    [HttpGet]
    public TemperatureDto Get()
    {
        var temperature = _temperatureSensorService.GetTemperature();
        _cacheService.AddTemperature(temperature);

        return temperature;
    }

    [SwaggerOperation(Summary = "Get Temperature History")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(List<TemperatureDto>), 200)]
    [HttpGet("History")]
    public List<TemperatureDto> GetHistory()
    {
        return _cacheService.GetTemperatureCache();
    }

    [SwaggerOperation(Summary = "Set Limit parametres")]
    [Produces("application/json")]
    [ProducesResponseType(200)]
    [HttpPost("Parametres")]
    public void SetParams([FromQuery] int minWarm,[FromQuery] int maxWarm)
    {
        _temperatureSensorService.ChangeLimits(minWarm, maxWarm);
        return ;
    }
}
