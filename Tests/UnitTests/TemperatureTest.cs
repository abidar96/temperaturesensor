using Moq;
using NUnit.Framework;
using TemperatureCaptor.Application.Services;
using TemperatureCaptor.Domain.Entities;
using TemperatureCaptor.Domain.Services;
using TemperatureCaptor.Infrastructure.Services;
using TemperatureCaptor.Infrastructure.TemperatureCaptorGenerator;

namespace UnitTests;

public class TemperatureTest
{
    private readonly Mock<ITemperatureCaptorGenerator> _generatorMock;
    private Mock<TemperatureService> _temperatureServiceMock;
    private ITemperatureSensorService _temperatureSensorService;


    public TemperatureTest()
    {
        _generatorMock = new Mock<ITemperatureCaptorGenerator>();
    }

    [SetUp]
    public void Setup() 
    {
        _temperatureServiceMock = new Mock<TemperatureService>();
        _temperatureSensorService = new TemperatureSensorService(_generatorMock.Object, _temperatureServiceMock.Object);

    }

    [Test]
    public void GetTemperatureShouldReturnCold()
    {
        // Arrange
        _generatorMock.Setup(x => x.GetTemperature()).Returns(10);

        //ACT
        var temperature = _temperatureSensorService.GetTemperature();
        
        //Assert
        Assert.IsNotNull(temperature);
        Assert.AreEqual(10, temperature.Value);
        Assert.AreEqual(TemperatureStatus.COLD.ToString(), temperature.Status);
    }

    [Test]
    public void GetTemperatureShouldReturnWarm()
    {
        // Arrange
        _generatorMock.Setup(x => x.GetTemperature()).Returns(25);

        //ACT
        var temperature = _temperatureSensorService.GetTemperature();

        //Assert
        Assert.IsNotNull(temperature);
        Assert.AreEqual(25, temperature.Value);
        Assert.AreEqual(TemperatureStatus.WARM.ToString(), temperature.Status);
    }

    [Test]
    public void GetTemperatureShouldReturnHot()
    {
        // Arrange
        _generatorMock.Setup(x => x.GetTemperature()).Returns(50);

        //ACT
        var temperature = _temperatureSensorService.GetTemperature();

        //Assert
        Assert.IsNotNull(temperature);
        Assert.AreEqual(50, temperature.Value);
        Assert.AreEqual(TemperatureStatus.HOT.ToString(), temperature.Status);
    }


    [Test]
    public void ChangeLimitsShouldReturnHot()
    {
        // Arrange
        _generatorMock.Setup(x => x.GetTemperature()).Returns(16);

        //ACT
        _temperatureSensorService.ChangeLimits(10, 15);
        var temperature = _temperatureSensorService.GetTemperature();

        //Assert
        Assert.IsNotNull(temperature);
        Assert.AreEqual(16, temperature.Value);
        Assert.AreEqual(TemperatureStatus.HOT.ToString(), temperature.Status);
    }

    [Test]
    public void ChangeLimitsShouldReturnCold()
    {
        // Arrange
        _generatorMock.Setup(x => x.GetTemperature()).Returns(45);

        //ACT
        _temperatureSensorService.ChangeLimits(46, 50);
        var temperature = _temperatureSensorService.GetTemperature();

        //Assert
        Assert.IsNotNull(temperature);
        Assert.AreEqual(45, temperature.Value);
        Assert.AreEqual(TemperatureStatus.COLD.ToString(), temperature.Status);
    }

    [Test]
    public void ChangeLimitsShouldReturnWarm()
    {
        // Arrange
        _generatorMock.Setup(x => x.GetTemperature()).Returns(10);

        //ACT
        _temperatureSensorService.ChangeLimits(5, 15);
        var temperature = _temperatureSensorService.GetTemperature();

        //Assert
        Assert.IsNotNull(temperature);
        Assert.AreEqual(10, temperature.Value);
        Assert.AreEqual(TemperatureStatus.WARM.ToString(), temperature.Status);
    }

    


}
