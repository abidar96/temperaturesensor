using Microsoft.AspNetCore.Mvc.Testing;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using TemperatureCaptor.Application;
using TemperatureCaptor.Application.Dtos;

namespace IntegrationTest
{
    public class TempuratureTest
    {
        private readonly HttpClient _client;

        public TempuratureTest()
        {
            var app = new WebApplicationFactory<Program>();
            _client = app.CreateClient();
        }

        [Test]
        public async Task GetTemperatureShouldReturn200()
        {

            //ACT
            var response = await _client.GetAsync("/api/Temperature");
            var content = await response.Content.ReadAsStringAsync();
            var temperature = JsonSerializer.Deserialize<TemperatureDto>(content);

            //Assert
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
            Assert.IsNotNull(temperature);
        }

        [Test]
        public async Task ChangeLimitsShouldReturn200()
        {
            //Arrange
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post,"/api/Temperature/parametres");
            requestMessage.Content = JsonContent.Create(new { minWarm = 5, maxWarm = 10 });

            //ACT
            var response = await _client.SendAsync(requestMessage);

            //Assert
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
        }


        [Test]
        public async Task GetHistoryShouldReturnEmptyList()
        {
            // Arrange
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, "/api/Temperature/History");

            //ACT
            var response = await _client.SendAsync(requestMessage);
            var content = await response.Content.ReadAsStringAsync();
            var history = JsonSerializer.Deserialize<List<TemperatureDto>>(content);

            //Assert
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(0, history.Count);
        }

        [Test]
        public async Task GetHistoryShouldReturnList()
        {
            for(int i = 0; i < 10; i++)
            {
                _ = await _client.GetAsync("/api/Temperature");
            }

            // Arrange
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, "/api/Temperature/History");

            //ACT
            var response = await _client.SendAsync(requestMessage);
            var content = await response.Content.ReadAsStringAsync();
            var history = JsonSerializer.Deserialize<List<TemperatureDto>>(content);

            //Assert
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(10,history.Count);
        }

        [Test]
        public async Task GetHistoryShouldReturnListWith15Elemnts()
        {
            for (int i = 0; i < 16; i++)
            {
                _ = await _client.GetAsync("/api/Temperature");
            }

            // Arrange
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, "/api/Temperature/History");

            //ACT
            var response = await _client.SendAsync(requestMessage);
            var content = await response.Content.ReadAsStringAsync();
            var history = JsonSerializer.Deserialize<List<TemperatureDto>>(content);

            //Assert
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(15, history.Count);
        }
    }
}