﻿namespace TemperatureCaptor.Domain.Entities;

public class Temperature
{
    public int Value { get; set; }

    public TemperatureStatus Status { get; set; }

}

public enum TemperatureStatus
{
    COLD = 0,
    WARM = 1,
    HOT = 2
}
