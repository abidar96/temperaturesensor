﻿using TemperatureCaptor.Domain.Entities;

namespace TemperatureCaptor.Domain.Services;

public interface ITemperatureService
{
    (int,int) Limits {get;set;}
    TemperatureStatus GetTemperatureStatus(int temperatureValue);
}
