﻿namespace TemperatureCaptor.Infrastructure.TemperatureCaptorGenerator;


// This class is used as an extern Service to get just the temperature sensor
public class TemperatureCaptorGenerator : ITemperatureCaptorGenerator
{
    public int GetTemperature()
    {
        Random random = new Random();
        return random.Next(0, 60);
    }
}
