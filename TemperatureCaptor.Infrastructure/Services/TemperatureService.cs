﻿using TemperatureCaptor.Domain.Entities;
using TemperatureCaptor.Domain.Services;

namespace TemperatureCaptor.Infrastructure.Services
{
    public class TemperatureService : ITemperatureService
    {
        public (int, int) Limits { get; set; }


        public TemperatureService()
        {
            Limits = (22, 40);
        }

        public TemperatureStatus GetTemperatureStatus(int temperatureValue)
        {
            // it's impossible to use switch no constant variables
            /*
            return temperatureValue switch
            {
                < 22 => TemperatureStatus.COLD,
                >= 22 and < 40 => TemperatureStatus.WARM,
                >= 40 => TemperatureStatus.HOT,
            };*/

            if (temperatureValue < Limits.Item1)
                return TemperatureStatus.COLD;
            else if (temperatureValue >= Limits.Item1 && temperatureValue <= Limits.Item2)
                return TemperatureStatus.WARM;
            else
                return TemperatureStatus.HOT;

        }
    }
}